# PyUOTools
This is a python library providing tools for mathematical programming

Install
=======
via pip from gitlab:

```python
pip install git+https://gitlab.com/jpluna/pyuotools.git
```
