import numpy as np
import pandas as pd

def fgBB(x, mode=0, context=None, fun=None, grad=None, fg=None):
    status = 0
    if fg is not None:
        fval, g = fg(x)
        if mode == 0:
            return fval, status
        elif mode == 1:
            return g, status
        elif mode == 2:
            return fval, g, status
    else:
        if mode == 0:
            fval = fun(x)
            return fval, status
        elif mode == 1:
            g = grad(x)
            return g, status
        elif mode == 2:
            fval = fun(x)
            g = grad(x)
            return fval, g, status
            

def numBB(x, mode=0, context=None, fun=None, delta=0.0001):
    val = fun(x)
    if mode == 0:
        return val, 0
    else:
        grad = np.zeros_like(x)
        xx = x[:]
        xx[0] += delta
        ff = fun(xx)
        grad[0] = (ff - val) / delta
        
        for i in range(1, len(x)):
            xx[i - 1] -= delta
            xx[i] += delta
            ff = fun(xx)
            grad[i] = (ff - val) / delta
        if mode == 1:
            return grad, 0
        elif mode == 2:
            return val, grad, 0

def checkBB(x, bb=None,fun=None, grad=None, fg=None, h=1e-5, tol=1e-3, showErrorIndex=True, useRandCoord=None):
    ''' 
    def checkBB(x, bb=None,f=None, g=None, fg=None, h=1e-5, tol=1e-3, showErrorIndex=True, useRandCoord=None):
        tol: 
        useRandCoord: size of random coordingate for testing. Default None: all coordintes will be tested.
    return: True/False
    '''
    import numpy as np
    n = len(x)

    if bb is None:
        if fun is None: 
            if fg is not None:
                bb = lambda x,mode=0: fgBB(x, mode=mode, context=None, fun=None, grad=None, fg=fg)
            else:
               raise TypeError('Missing function informatin')
        elif grad is not None:
            bb = lambda x,mode=0: fgBB(x, mode=mode, context=None, fun=fun, grad=grad, fg=None)
        else:
           raise TypeError('Missing gradient  function')

    fx, g, status = bb(x, mode=2)
    gnum = np.zeros_like(x)
    aux = np.zeros_like(x)

    if useRandCoord is None: 
        coord = list(range(n))
    else:
        coord = []
        while len(coord)< useRandCoord:
            cc = np.random.randint(1,n)
            if not cc in coord:
                coord.append(cc)

    for i in coord:
        aux[:]=0
        aux[i] = 1
        fxx, status = bb(x + h * aux, mode = 0)
        gnum[i] = (fxx - fx) / h

    vErrror = np.abs(g[coord] - gnum[coord])
    error = max(vErrror)
    if (showErrorIndex and ( error > tol)): 
        print('Grandient entries with possible errors:\n')
        ind = vErrror>tol
        nInd = np.array(coord)[ind]
        tab = pd.DataFrame( {'index':nInd , 'grad':g[nInd], 'numGrad':gnum[nInd], 'error': vErrror[ind]})
        tab = tab.set_index('index')
        print(tab)
    return (error <= tol)

        


def checkNSBB(x, fun=None, subgrad=None, bb=None, bbContext=None, nSample=5, tol=1e-5):
    '''
    Checks if the  result of the subgradient function 'subgrad(x) --> s, status'  corresponds to a 
    subgradient of 'fun(x) --> fval, status'.  If 'bb' is provided, 'fun' and 'subgrad' are discarded.
    
    def checkNSBB(x, fun=None, subgrad=None, bb=None, bbContext=None, nSample=5, tol=1e-5):
        return: True o False, according if the subgradient is computed correctly
        or not.
    '''
    import numpy as np
    if bb is None:
        if ((fun is not None) and (subgrad is not None)):
            bb = lambda x, mode, context=None: fgBB(x, mode=mode, context=context, fun=fun, grad=subgrad)
        else:
            print('Error: (fun and subgrad) or bb should be provided')

    fx, g, status = bb(x, mode=2, context=bbContext)
    diff = np.zeros(nSample)
    if status != 0:
        print('black box error: ' + int(status) + '\n')
        return False
    else: 
        if len(g) != len(x):
            print('black box error: length of  the returned subgradient is {}. It should be {} \n'.format(len(g), len(x)))
            return False
        for i in range(nSample):
            d = 2 * np.random.randn(len(x))  - 1
            y =  x + d
            fy, status = bb(y, mode=0, context=bbContext)
            if status == 0:
                diff[i] = fy - fx - np.dot(g, d)
        return all(diff > -tol)

def plot(fun=None, bb=None, bbContext=None, fg=None, x=None, y=None, ax=None, paths=None, files=None, labels=None, colors=None, save=None, alpha=1, linewidth=1, gridSize=100, curves=False, levels=50, equalAxis=False):
    '''
def plot(fun=None, bb=None, fg=None, x=None, y=None,  files=None, labels=None, colors=None, save=None, alpha=1, linewidth=1, gridSize=100, curves=False, levels=50, equalAxis=False):
    Plots the level curves of the function described by fun or bb. At least one of them should be provided. In case both are provided 'bb' has precedence.
        fun(x): function that returns a number
        bb(x,mode=0, context=None): returns for mode==0 the pair (value_function, status)
    In the plot is also included a list of sequences described by path and files. Thus, at least some of the arguments x, y, path or files:
        x: [x_min, x_max]
        y: [y_min, y_may]
        path: is a list of lists. Each element  is of  the form [xx, yy], where xx is a list
        the firts entries oa list of points and yy is the a list of thre corresponding second
        coordinates
        files: list of log files that are produced by the method 'descent 
    save: string for a figure name.
    '''
    import numpy as np
    import pandas as pd
    import matplotlib.pyplot as plt
    
    if fun is None:
        if ((bb is None) and (fg is None)):
            print('Error: fun or bb must be provided')
        else:
            if bb is not None:
                fun = lambda x: bb(x, mode=0, context=bbContext)[0]
            else:
                fun = lambda x: fg(x)[0]

    path = None

    




    if files is not None:
        fpath=[] 

        for ff in files:
            tab = pd.read_csv(ff, index_col=0, header=None)
            # tab = tab[tab.index.map(lambda x: 'x' in x).values]
            aux = [tab[1].values, tab[2].values]
            fpath.append(aux)

        if ((labels is None) or (len(labels) < len(path))):
            labels = [nn.split('.')[0] for nn in files]

        if colors is None: colors = ['r', 'k', 'g', 'm'] 
        if len(colors) < len(fpath): colors = (len(fpath) // len(colors) + 1) * colors 
        if path is None: 
            path = list(zip(fpath, colors, labels))
        else:
            path = path + list(zip(fpath, colors, labels))

    if paths is not None:
        fpath = []

        for ss in paths:
            aux = np.array(ss).T
            aux = [aux[0], aux[1]]
            fpath.append(aux)

        slabels = [ '_path-{}'.format(i) for i in range(1, len(fpath) + 1)]

        if colors is None: colors = ['r', 'k', 'g', 'm'] 

        if len(colors) < len(fpath): colors = (len(fpath) // len(colors) + 1) * colors 
        if path is None: 
            path = list(zip(fpath, colors, slabels))
        else:
            path = path + list(zip(fpath, colors, slabels))


    xexlist = [] # minimum and maximum of x entties of each path
    yexlist = [] # minimum and maximum of y entties of each path
    if ax is None:
        fig = plt.figure()
        ax = fig.gca()
    ax.cla()
    if path is not None: 
        for aux, cc, ll in path: 
            xexlist.append(min(aux[0]))
            xexlist.append(max(aux[0]))
            yexlist.append(min(aux[1]))
            yexlist.append(max(aux[1]))
            ax.plot(aux[0], aux[1], 'o--', color=cc, linewidth=linewidth, label=ll)
            ax.plot(aux[0][0], aux[1][0], 'go')
            ax.plot(aux[0][-1], aux[1][-1], 'ko')

    ax.legend()

    if ((x is None) and (y is not None)):
        x = y

    if ((y is None) and (x is not None)):
        y = x

    if x is not None: 
        xmin = min(x) 
        xmax = max(x)
        # xexlist.append(min(x)) 
        # xexlist.append(max(x))
    elif path is not None:
        xmin = min(xexlist)
        xmax = max(xexlist)
        lx = xmax - xmin
        xmin = xmin - 0.15 * lx
        xmax = xmax + 0.15 * lx
    else:
        xmin=0
        xmax=1
    ax.set_xlim(xmin, xmax)

    if y is not None:
        ymin = min(y) 
        ymax = max(y)
        # yexlist.append(min(y)) 
        # yexlist.append(max(y))
    elif path is not None:
        ymin = min(yexlist)
        ymax = max(yexlist)
        ly = ymax - ymin
        ymin = ymin - 0.15 * ly
        ymax = ymax + 0.15 * ly
    else:
        ymin=0
        ymax=1
    ax.set_ylim(ymin, ymax)

    # if len(xexlist)>0  or len(yexlist)>0:
        # if len(xexlist)>0:
            # xmin = min(xexlist)
            # xmax = max(xexlist)
            # if len(yexlist)>0:
                # ymin = min(yexlist)
                # ymax = max(yexlist)
            # else:
                # ymin= xmin
                # ymax = xmax
        # else:
            # ymin = min(yexlist)
            # ymax = max(yexlist)
            # xmin= ymin
            # xmax = ymax
    # else:
        # ymin = 0
        # xmin = 0
        # ymax = 1
        # xmax = 1

    # lx = xmax - xmin
    # ly = ymax - ymin
    # xa = [xmin - (0.15 * lx), xmax + (0.15 * lx)]
    # ya = [ymin - (0.15 * ly), ymax + (0.15 * ly)]
    # xx = np.arange(xa[0], xa[1], lx/50)
    # yy = np.arange(ya[0], ya[1], ly/50)
    xx = np.linspace(xmin, xmax, gridSize)
    yy = np.linspace(ymin, ymax, gridSize)
    XX, YY = np.meshgrid(xx, yy)
    # Note the peculiar estructure of XX and YY
    ## XX has all rows equal (and so constrant columns)
    ## YY has  constant rows (and so all columns are the same)
    ZZ = list()
    for xy in zip(XX, YY):
        ZZ += [[fun(np.array([xxx, xy[1][0]])) for xxx in xy[0]]]

    if curves:
        ax.contour(XX, YY, np.array(ZZ), levels, alpha=alpha)
    else: 
        ax.contourf(XX, YY, np.array(ZZ), levels, alpha=alpha)
    # plt.axis('equal')
    if equalAxis: ax.axis('equal')

    if save is not None:
        fig.savefig(save)
    # plt.show()

    return ax

def plot3D(fun=None, bb=None, x=None, y=None, save=None): 
    '''
    plot3D(bb, x, y, save=None)
        fun: funtion for plotting
        bb: blackBox associated to fun
        x=[x_min, x_max], if None, x=[0,1]
        y=[y_min, y_max], if None, y=[0,1]
        save: str
            file name for saving figure
    return: fig
    '''
    if fun is None:
        if bb is None:
            print('Error: fun or bb must be provided')
        else:
            fun = lambda x: bb(x, mode=0)[0]
    from mpl_toolkits.mplot3d import Axes3D
    import matplotlib.pyplot as plt
    if x is None: x = [0,1]
    if y is None: y = [0,1]
    xx = np.linspace(x[0], x[-1], 50)
    yy = np.linspace(y[0], y[-1], 50)
    nx = len(xx)
    ny = len(yy)
    [X, Y] = np.meshgrid(xx, yy)
    Z = np.zeros((nx, ny))
    for i in range(nx):
        for j in range(ny):
            Z[i,j] = fun(np.array([xx[i], yy[j]]))
    fig = plt.figure()
    # ax = fig.gca(projection='3d')
    ax = fig.add_subplot(projection='3d')
    surf = ax.plot_surface(X, Y, Z)
    if save is not None:
        plt.savefig(save)
    return fig



def plotLog(files,axf=None, axg=None, color=None, save=None, bokeh=False, layout=None):
    '''
    layout: None, 'vertical' or 'horizontal'
    '''
    
    import pandas as pd
    import os
###
    fval = {}
    grad = {}
    baseNames=[]
###
    # import pdb 
    for ff in files:
        baseName = os.path.splitext(ff)[0]
        baseNames.append(baseName)
        tabAux = pd.read_csv(ff, header=None, names=['iter', 'fval', 'grad'], comment='#')
        tabAux['iteration'] = tabAux['iter'].apply(lambda x: int(x.split('=')[-1][:-1])).to_frame()
        tabAux = tabAux.set_index('iteration')

        # pdb.set_trace()
        for dnn, nn in [(fval, 'fval'), (grad, 'grad')]: 
            dnn.update({baseName: tabAux[nn].apply(lambda x: float(x.split(':')[-1]))})
    # pdb.set_trace()

    # import pdb 
    # pdb.set_trace()
    fval = pd.DataFrame(fval)
    grad = pd.DataFrame(grad)
    # print(fval)
###
    if bokeh:
        print('using bokeh...')
        #using bokeh
        import bokeh.plotting as bkplt
        import bokeh.models as bkm
        from bokeh.palettes import Category10
        from bokeh.models.tools import HoverTool
        import itertools
        from bokeh.layouts import row, column
        import bokeh.io as bkio

        try:
            bkio.output_notebook()
            notebook_handle=True
        except:
            notebook_handle=False

        colors = itertools.cycle(Category10[10])
        fvalS = bkm.ColumnDataSource(fval)
        gradS = bkm.ColumnDataSource(grad)

        if layout is None:
            width=600
            height=600
        elif layout.lower() == 'horizontal':
            width=350
            height=350
        else:
            width=400
            height=400
            

        
        fp = bkplt.figure(plot_width=width, plot_height=height, title='Function Value')
        gp = bkplt.figure(plot_width=width, plot_height=height,title='Gradient Infinite Norm')

        for cc in fval:
            color = next(colors)
            fp.line(x='iteration', y=cc, line_dash=[6, 6], source=fvalS, color=color, line_width=3, legend_label=cc, alpha=0.7)
            fpAux = fp.circle(x='iteration', y=cc, source=fvalS, color=color, size=10, legend_label=cc)
            fp.add_tools(HoverTool(renderers=[fpAux], tooltips=[('iteration', '@iteration'), ('fval', cc.join(['@{', '}']))], mode='vline'))

            gp.line(x='iteration', y=cc, line_dash=[6, 6], source=gradS, color=color, line_width=3, legend_label=cc, alpha=0.7)
            gpAux = gp.circle(x='iteration', y=cc, source=gradS, color=color, size=10, legend_label=cc)
            gp.add_tools(HoverTool(renderers=[gpAux], tooltips=[('iteration', '@iteration'), ('grad', cc.join(['@{', '}']))], mode='vline'))
        if layout is None: 
            bkplt.show(fp, notebook_handle=notebook_handle) 
            bkio.show(fp, notebook_handle=notebook_handle) 
            bkplt.show(gp, notebook_handle=notebook_handle)
        elif layout.lower() == 'horizontal':
            p = row(fp, gp)
            bkplt.show(p, notebook_handle=notebook_handle)
        else:
            p = column(fp, gp)
            bkplt.show(p, notebook_handle=notebook_handle)

    else:
        # using matplitlib
        print('using matplotlib...')
        import matplotlib.pyplot as plt
        if axf is None:
            fvalfig = plt.figure()
            axf = fvalfig.gca() 
        fval.plot(ax=axf, style='--*', title='Function Value')
        if axg is None:
            gradfig = plt.figure()
            axg = gradfig.gca()
        grad.plot(ax=axg, style='--*', title='Gradient Infinite Norm')
        # plt.show()
        return axf, axg
    
def plotNsoLog(logPath):
    '''
    layout: None, 'vertical' or 'horizontal'
    '''
    
    import pandas as pd
    import os

###
    fval = {}
    baseNames=[]
###
    ff = logPath

    baseName = os.path.splitext(ff)[0]
    baseNames.append(baseName)
    tabAux = pd.read_csv(ff, header=None, names=['iter', 'serious','sn', 'delta', 'fval', 'fz', 'aProxVal', 'mu', 'bundleSize'], delimiter=',', comment='#')
    tabAux['iteration'] = tabAux['iter'].apply(lambda x: int(x.split('=')[-1][:-1]))
    tabAux = tabAux.set_index('iteration')

    for kk in ['delta', 'fval', 'fz', 'aProxVal', 'mu']: 
        tabAux[kk]  = pd.to_numeric( tabAux[kk].apply(lambda x: str(x).split('=')[-1]) , errors='coerce')

###
    # using matplitlib
    print('using matplotlib...')
    import matplotlib.pyplot as plt

    fig, axs = plt.subplots(3)

    tabAux[[ 'fz', 'aProxVal', 'fval']].plot(ax=axs[0], style='--*')
    tabAux[['delta']].plot(ax=axs[1], style='--*')
    tabAux[['mu']].plot(ax=axs[2], style='--*')
    fig.savefig('{}.png'.format(baseName))
    # print(tabAux)
###


### penalizing funtions


def penalization(x, kcsub=None, eqInd=None, ineqInd=None, lb=None, ub=None, sigma=2):
    '''
    penalization(x, kcsub, eqInd=None, ineqInd=None, lb=None, ub=None, sigma=2):
    '''
    val = 0
    if kcsub is not None:
        if eqInd is not None:
            aux = []
            for k in eqInd:
                c, status = kcsub(x, k)
                if status != 0:
                    print('Error: constraint function returner status = {}\n'.format(status))
                    return 0, status
                if c > 0:
                    aux.append((c, 1, k))
                elif c < 0:
                    aux.append((-c, -1, k))
            if len(aux) > 0:
                h, signH, keys = zip(*aux)
                h = np.array(h)
                signH = np.array(signH)
            else:
                h = np.array([])
                signH = np.array([])
                keys = []
        else:
            h = np.array([])
            signH = np.array([])
            keys = []

        if ineqInd is not None:
            aux = []
            for k in ineqInd:
                c, status = kcsub(x, k)
                if status != 0:
                    print('Error: constraint function returner status = {}\n'.format(status))
                    return 0, status
                if c > 0:
                    aux.append((c, k))
            if len(aux) > 0:
                g, keys = zip(*aux)
                g = np.array(g)
            else:
                g = np.array([])
                keys = []
        else:
            g = np.array([])
            keys = []
    else:
        h = np.array([])
        signH = np.array([])
        keys = []
        g = np.array([])
        keys = []


    if lb is not None:
        ind = (lb>-np.inf) * (lb<np.inf)
        lbb = lb[ind] - x[ind]
        lbb = lbb[lbb > 0]
    else:
        lbb = np.array([])

    if ub is not None:
        ind = (ub>-np.inf) * (ub<np.inf)
        ubb =  x[ind] - ub[ind]
        ubb = ubb[ubb > 0]
    else:
        ubb = np.array([])
    v = np.concatenate([h,g,lbb,ubb])

    if sigma == 2:
        val += 0.5 * np.dot(v,v)
    else:
        val += np.sum(np.exp(sigma * np.log(v))) / sigma
    status = 0
    return val, 0

