import numpy as np
from . import tools as tl
from . import ucFuncCodes as fc

description = {
        'rosenbrock': ''' 
    $f(x) = sum_{i=1}^{n-1} 100(x_{i+1} - x_i^2)^2 +(1-x_i)^2$
    The dimension is obtained from the argument x
    ''',
    'sugar': '''
    The dimension is obtained from the argument x
    $f(x) = sum(x) * exp(-6 |x|^2)$
    hessian available with mode=3.
    ''',
    'quad':''' 
    $f(x) = 0.5 <Hx,x>$
    context=H: semidefine matrix. Defalt None. If None, problem dimension is obtained from argmument x, and H is set to the indentity matrix.
    hessian (H) is available with mode=3
    ''',
    'perturbedQuad':'''
    $f(x) =\sum_{i=1}^n i x_i^2 +\frac{1}{100}(\sum_{i=1}^n x_i)^2 $
    Dimension is obtained from argument x.
    hessian available with mode=3
    ''',
    }

codes = list(description.keys())

def getContext(code, base=None, updateContext=None):
    cc = base
    if code in ['rosenbrock', 'sugar', 'perturbedQuad']: #in this case, the context mainly defines just the dimension
        if cc is None:
            cc = {'n': 2}
        if updateContext is not None:
            cc.update(updateContext)
    elif code == 'quad': 
        if cc is None: 
            cc = {'n': 2, 'H':np.eye(2)}
        if updateContext is not None:
            cc.update(updateContext)
    else:
        cc = {}
    return cc

_codeF={
        'rosenbrock': fc.rosenbrock,
        'sugar': fc.sugar,
        'quad': fc.quad,
        'perturbedQuad': fc.perturbedQuad,
        }


class problem(tl.basicProblem):
    def __init__(self,code):
        self._codes = codes
        self._description= description
        self._getContext = getContext
        self._codeF=_codeF
        tl.basicProblem.__init__(self,code)

