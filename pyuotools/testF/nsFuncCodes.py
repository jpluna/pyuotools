import numpy as np
### coding the BBs

def maxq(x, mode=0, context=None):
    '''
    f(x) = max xi^2
    the subgradient is 2*xi*ei, where i is the first element of argmax{xi^2}
    '''
    status = 0
    x2 = x * x
    argmax = np.argmax(x2)
    if mode == 0:
        return x2[argmax], status
    elif mode == 1:
        g = np.zeros_like(x)
        g[argmax] = 2 * x[argmax]
        return g, status
    elif mode == 2:
        g = np.zeros_like(x)
        g[argmax] = 2 * x[argmax]
        return x2[argmax], g, status

def chainedLQ(x, mode=0, context=None):
    '''
    f(x) = \sum_{0,..n-2}\max{-xi-x(i+1), -xi-x(i+1) + (xi^2 +x(i+1)^2 -1 )}  
    the subgradient is 2*xi*ei, where i is the first element of argmax{xi^2}
    '''
    status = 0
    xx = -x[:-1] - x[1:] 
    yy = (x[:-1] ** 2) + (x[1:] ** 2) - 1
    yy = np.select([yy>0], [yy])
    
    if mode == 0:
        return sum(xx + yy), status
    elif mode == 1:
        g = -2 * np.ones_like(x)
        g[0] = -1
        g[-1] = -1
        for i in range(len(yy)):
            if yy[i] > 0:
                g[i] += 2 * x[i]
                g[i + 1] += 2 * x[i + 1]
        return g, status
    elif mode == 2: 
        g = -2 * np.ones_like(x)
        g[0] = -1
        g[-1] = -1
        for i in range(len(yy)):
            if yy[i] > 0:
                g[i] += 2 * x[i]
                g[i + 1] += 2 * x[i + 1]
        return sum(xx + yy), g, status

def instabcp(x, mode=0, context=None):
    '''
    f(x) = max {1, -1+2*lambda +|x|}
    the subgradient is  0, if |x|< 2(1-lambda), otherwise x/|x|
    '''
    # print(10*'=====')
    # print(context)

    status = 0
    l = context['lambda']
    normx =  np.sqrt(np.dot(x,x))
    v2 = normx - 1 + 2*l
    if mode %2 ==0:
        val = 1 if (v2<1) else v2  
    if mode >0:
        g = np.zeros_like(x) if ((v2<1) or (normx<1e-8)) else (x/normx)
    if mode == 0:
        return val, status
    elif mode == 1:
        return g, status
    elif mode == 2:
        return val, g, status

def mxhilb(x, mode=0, context=None):
    status = 0
    n = len(x)
    J = 1+np.arange(n)
    vals = [ np.abs(x/(i+J-1)).sum() for i in range(1,n+1)]
    argmax = np.argmax(vals)

    if mode %2 ==0:
        val = vals[argmax]
    if mode >0:
        i = argmax+1
        g = np.sign(x)/(i+J-1)
    if mode == 0:
        return val, status
    elif mode == 1:
        return g, status
    elif mode == 2:
        return val, g, status

def maxquad(x, mode=0, context=None):
    status = 0
    A = context['A']
    b = context['b']
    vals = [np.dot(np.dot(Ai,x) + bi,x) for Ai, bi in zip(A, b)]
    argmax = np.argmax(vals)

    if mode %2 ==0:
        val = vals[argmax]
    if mode >0:
        g = 2*np.dot(A[argmax],x) + b[argmax]
    if mode == 0:
        return val, status
    elif mode == 1:
        return g, status
    elif mode == 2:
        return val, g, status
