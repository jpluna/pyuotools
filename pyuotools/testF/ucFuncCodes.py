import numpy as np
def rosenbrock(x, mode=0, context=None):
    status = 0
    n = len(x)
    if mode == 0:
        if (context is not None) and ('fun' in context): context['fun'] += 1
        return   100 * sum((x[1:] - (x[:-1] ** 2)) ** 2)  + sum((1 - x[:-1]) ** 2), status
    elif mode == 1:
        if (context is not None) and ('grad' in context): context['grad'] += 1
        g = np.zeros(n)
        for i in range(n-1):
            g[i] += 400 * x[i] * ((x[i] ** 2) - x[i + 1]) + (2 * (x[i] - 1))
            g[i + 1] += 200 * (x[i + 1]  - (x[i] ** 2)) 
        return g, status
    elif mode == 2:
        fval =  100 * sum((x[1:] - (x[:-1] ** 2)) ** 2)  + sum((1 - x[:-1]) ** 2)
        g = np.zeros(n)
        for i in range(n-1):
            g[i] += 400 * x[i] * ((x[i] ** 2) - x[i + 1]) + (2 * (x[i] - 1))
            g[i + 1] += 200 * (x[i + 1]  - (x[i] ** 2)) 
        if (context is not None) and ('fun' in context): context['fun'] += 1
        if (context is not None) and ('grad' in context): context['grad'] += 1

        return fval, g, status

def sugar(x, mode=0, context={}): 
    '''
    f(x) = sum(x) * exp(-6 |x|^2)
    hessian available with mode=3
    '''
    status = 0
    exp6x = np.exp(-6 * np.dot(x,x))
    fval = x.sum() * exp6x
    if mode == 0:
        return fval, status
    else:
        grad = exp6x * np.ones_like(x) -12 * fval *x
        if mode == 1:
            return grad, status
        elif mode == 2:
            return fval , grad, status
        elif mode == 3:
            hess = -12 * ( np.diag(fval * np.ones_like(x))  + x.reshape(-1,1) * grad +exp6x*np.ones_like(x).reshape(-1,1)*x)
            return hess, 0

def quad(x, mode=0, context=None):
    '''
    f(x) = 0.5 <Hx,x>
    hessian available with mode=3
    '''
    if (context is not None) and ( 'H' in context):
        H = context['H']
    else: 
        H = np.eye(len(x))
    if mode <3:
        grad = np.dot(H,x)
        if mode == 0:
            return 0.5 * np.dot(grad, x), 0
        elif mode ==1:
            return grad, 0
        elif mode == 2:
            return 0.5 * np.dot(grad, x), grad,  0
    elif mode == 3:
        return H, 0
        
def perturbedQuad(x, mode=0, context=None):
    '''
    $f(x) =\sum_{i=1}^n i x_i^2 +\frac{1}{100}(\sum_{i=1}^n x_i)^2 $
    hessian available with mode=3
    '''
    n = len(x)
    I = np.arange(1, n + 1)
    if mode < 3:
        Ix = I * x
        sumx = x.sum()
        if mode > 0:
            grad = 2 * Ix + 0.02 * sumx * np.ones_like(x)
            if mode == 1:
                return grad, 0
            elif mode == 2:
                return 0.5 * grad.dot(x), grad,  0

        elif mode ==0:
            return Ix.dot(x) + (sumx * sumx)/100, 0
    elif mode == 3:
        return 2 *  np.diag(I) + 0.02 * np.ones((n,n)), 0
