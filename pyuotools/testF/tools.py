class basicProblem():
    _codes=None
    _description=None
    _getContext=None
    _codeF=None
    code=None
    n=None,
    description=None
    __bb__=None
    context=None
    def __init__(self,code):
        if code in self._codes:
            self.code = code
            self.context = self._getContext(code)
            self.n = self.context['n']
            self.description = self._description[code]
            self.__bb__ =  self._codeF[code]
        else:
            print('Code not found in list')
    def updateContext(self, data):
        cc = self._getContext(self.code, base=self.context, updateContext=data)
        self.context = cc
        self.n=self.context['n']
    def bb(self,x,mode=0, context=None):
        '''
        context is dummy variable. It will not make any difference. it is included in the definition just to make sure the problem will be used in puo and pynso structures
        '''
        # print(self.context)
        res = self.__bb__(x, mode=mode, context=self.context)
        return res
    def __repr__(self):
        ss = '\n\n'.join([self.code, self.description, str(self.context)])
        return ss


