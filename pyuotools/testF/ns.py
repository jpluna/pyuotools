import numpy as np
from . import tools as tl
from . import nsFuncCodes as fc

description = {
        'maxq': ''' parameters: n (interger) dimension of the problem. Default 2. Objective function: $ f(x) = \max_{1\leq i\leq n} x_i^2$. ''',
        'chainedlq': '''parameters: n (interger) dimension of the problem. Default 2. Objective function: 
        $ f(x) =  \sum_{i=1}^{n-1} -x_i - x_{i+1} + \left[ x_i^2 + x_{i+1}^2-1 \right]^+ 
        ''',

        'instabcp': ''' parameters: 
        -  n (interger) dimension of the problem. Default 2.
        - lambda (float) real number. default 0.
        Objective function:
                \[ 
                f(x) =\max\{1, -1+2\lambda + \|x\|\}
                \]
                ''',

        'mxhilb': ''' parameters: 
        -  n (interger) dimension of the problem. Default 2.
        Objective function:
        	\[
		f(x) = \max_{1\leq i\leq n} \sum_{j=1}^n \left|\frac{x_j}{i+j-1|} \right| 
        \]
                ''',

        'maxquad': ''' parameters: 
        -  n (interger) dimension of the problem. Default 10.
        - np (integer) number function for computing max. default 5.
        - A (numpy array (np, n,n ): twice the hessian of the component functon
        - b (numpy array (np, n ): linear  of the component functon
        Objective function:
        	\[
		f(x) = \max_{1\leq i\leq np}  x^TA_jx + b^T_jx
        \]
                ''',
        }

codes = list(description.keys())

def getContext(code, base=None, updateContext=None):
    cc = base
    if code in ['maxq', 'chainedlq', 'mxhilb']: #in this case, the context mainly defines just the dimension
        if cc is None:
            cc = {'n': 2}
        if updateContext is not None:
            cc.update(updateContext)

    elif code == 'instabcp': 
        if cc is None: 
            cc = {'n': 2, 'lambda':0}
        if updateContext is not None:
            cc.update(updateContext)
    elif code == 'maxquad': 
        if cc is None: 
            cc = {'n': 10, 'np':5}
        if updateContext is not None:
            cc.update(updateContext)
        if (updateContext is None) or ('A' not in updateContext):
            n = cc['n']
            nnp = cc['np']
            A = np.zeros((nnp,n,n))
            Aux = np.zeros((n,n))

            k = 1+np.arange(n)
            for i in range(n):
                Aux[i,i+1:] = np.exp((i+1)/k[i+1:])*np.cos((i+1)*k[i+1:])
                Aux[i+1:,i] = Aux[i,i+1:]
            for j in range(nnp):
                A[j] = np.sin(j+1)*Aux + np.abs(np.sin(j+1))*np.diag((k/n) + np.abs(Aux).sum(axis=0))
            cc['A'] = A

        if (updateContext is None) or ('b' not in updateContext):
            b = np.zeros((nnp, n))
            i = 1+np.arange(n)
            for j in range(nnp):
                b[j,:] =  np.exp(i/(j+1))*np.sin(i*(j+1))
            cc['b'] = b
                
    else:
        cc = {}
    return cc

_codeF={ 
        'maxq': fc.maxq,
        'chainedlq': fc.chainedLQ,
        'instabcp': fc.instabcp, 
        'mxhilb': fc.mxhilb, 
        'maxquad': fc.maxquad, 
        }

class problem(tl.basicProblem):
    def __init__(self,code):
        self._codes = codes
        self._description= description
        self._getContext = getContext
        self._codeF=_codeF
        tl.basicProblem.__init__(self,code)
