import numpy as np

bData={'a':[0, np.log(2)],
       'b': [0, 1],
       'c':[-0.5, 0],
       'd':[0, 1/8]
        }

def smooth(x, tau=1e-4, kernel='a'):
    if kernel == 'a':
        aux =  1 + np.exp(-x / tau)
        return x + tau * np.log(aux), 1.0/ aux
    elif kernel == 'b':
        aux = np.sqrt( x * x + 4 * tau * tau)
        return (x + aux) / 2, (1 + (x / aux)) / 2

    elif kernel == 'c':
        val = np.zeros_like(x)
        deriv = np.zeros_like(x)
        pos = x>=0
        mTau = x<= tau
        iAux = pos & mTau
        
        val[iAux] = (x[iAux] * x[iAux] ) / (2 * tau)
        deriv[iAux] = x[iAux] / tau

        iAux = ~mTau
        val[iAux] = x[iAux] - tau/2
        deriv[iAux] = 1
        return val, deriv 
    
    elif kernel == 'd':
        val = np.zeros_like(x)
        deriv = np.zeros_like(x)
        nInd = x< -tau/2
        pInd = x> tau/2
        
        val[pInd] = x[pInd]
        deriv[pInd] = 1

        iAux = (~nInd) & (~pInd)
        aux = (x[iAux] + tau/2)
        val[iAux] = aux * aux / (2 * tau)
        deriv[iAux] = aux/tau
        return val, deriv 
