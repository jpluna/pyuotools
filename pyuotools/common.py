import os

class result_class():
    time=None
    x=None
    fval=None
    cIter=None
    status=None
    state=None
    extraInfo=None
    optimalityMeasure=None
    def __init__(self, x=None, fval=None,  cIter=None,status=None, time=None, state=None, extraInfo=None):
        self.x=x
        self.fval=fval
        self.cIter=cIter
        self.status=status
        self.time=time
        self.state=state
        self.extraInfo=extraInfo

    def __repr__(self):
        l1 = ['{} = {}'.format(key, val) for key, val in self.__dict__.items() if ((key not in ['x','g','extraInfo', 'state']) and (val is not None))]
        if self.state is not None:
            l1.append('\n')
            ss = 'Algorithm State:'
            l1.append(ss)
            l1.append(len(ss)*'=')
            l1.append(self.state.__repr__()) 
        return '\n'.join(l1)
    

class algorithmBasicState(): 
    context=None

    x0=None
    f0=None 
    g0=None

    xk=None
    fk=None
    gk=None

    optimalityMeasure=None
    optimalityMeasure0=None


    cIter=None

    initCallBackFun=None 
    iterCallBackFun=None 
    endCallBackFun=None 

    xLogFilePath=None
    globalLogFilePath=None

    # fval=None
    # x=None
    # g=None
    # gInfNorm=None

    def __init__(self, 
            x0,
            f0=None,
            g0=None,

            context=None, 
            initCallBackFun=None, 
            callBackFun=None, 
            endCallBackFun=None, 
            xLogFilePath=None, 
            logFilePath=None): 
    # def __init__(self, xLog=None, Log=None): 

        self.x0=x0
        self.f0=f0
        self.g0=g0

        self.context = context

        self.initCallBackFun = initCallBackFun
        self.callBackFun = callBackFun
        self.endCallBackFun = endCallBackFun

        self.xLogFilePath=xLogFilePath
        self.globalLogFilePath=logFilePath

        self.cIter = 0

        # erasing old files
        for ff in [self.xLogFilePath, self.globalLogFilePath]:
            if ff is not None:
                if os.path.isfile(ff): 
                    os.remove(ff)

    def initCallBack(self): 
        if self.initCallBackFun is not None: 
            self.initCallBackFun(self) 

    def callBack(self): 
        if self.callBackFun is not None: 
            self.callBackFun(self) 

    def endCallBack(self): 
        if self.endCallBackFun is not None: 
            self.endCallBackFun(self) 


    def __repr__(self): 
        return  '\n'.join(['{} = {}'.format(key, val) for key, val in self.__dict__.items() if isinstance(val, (int, float,str))])

    def writeXLog(self, mark='', vector=None):
        if vector is None: 
            vector = self.xk
        if self.xLogFilePath is not None:
            ff = open(self.xLogFilePath, 'a')
            aux = ','.join([str(xi) for xi in vector]) 
            if mark:
                message = '{},{},{}'.format(self.cIter, mark, aux)
            else:
                message = '{},{}'.format(self.cIter,  aux)

            ff.write(message + '\n')
            ff.close()

    def writeLog(self, message=None, sharp=True, includeIter=True):
        if self.globalLogFilePath is not None:
            ff = open(self.globalLogFilePath, 'a')
            if message is None: 
                ff.write('[k={}], fval: {}, grad inf-norm: {}\n'.format(self.cIter, self.fk, self.optimalityMeasure))
            else:
                if sharp:
                    mAux = message.replace('\n', '\n#')
                else:
                    mAux = message
                ff.write( sharp * '#' + includeIter * ' [k={}], '.format(self.cIter) +  '{}\n'.format(mAux))
            ff.close()

    def writeTag(self, message, tag=''):
        self.writeLog(message='[{}] {}'.format(tag, message), sharp=True, includeIter=True)
